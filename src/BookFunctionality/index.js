import React from 'react';
import Increase from '../assets/increase.svg'
import Decrease from '../assets/decrease.svg'

require('../BookingLayout/booking.scss')

class BookingFunctionality extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bookingResults: {
                rooms: 1,
                adults: 1,
                children: 3,
            }
        };
    }

    updateState = (key, control) => {
        if (key === 'rooms') {
            this.handleRooms(control)
        } else if (key === 'adults') {
            this.handleAdults(control)
        } else if (key === 'children') {
            this.handleChildren(control)
        }
    }

    handleRooms = (control) => {
        const { bookingResults } = this.state
        var currentRoomValue = bookingResults['rooms']

        if (control === 'increase' && currentRoomValue < 5) {
            currentRoomValue = currentRoomValue + 1
            if (bookingResults['adults'] < currentRoomValue) {
                bookingResults['adults'] = currentRoomValue
            }

            if ((bookingResults['adults'] + bookingResults['children']) > (currentRoomValue * 4)) {
                var toDecreaseInAdultsAndChildren = (bookingResults['adults'] + bookingResults['children']) - (currentRoomValue * 4)
                if (bookingResults['children'] >= toDecreaseInAdultsAndChildren) {
                    bookingResults['children'] = bookingResults['children'] - toDecreaseInAdultsAndChildren
                } else {
                    toDecreaseInAdultsAndChildren = toDecreaseInAdultsAndChildren - bookingResults['children']
                    bookingResults['children'] = 0
                    bookingResults['adults'] = bookingResults['adults'] - toDecreaseInAdultsAndChildren
                }
            }
        } else if (control === 'decrease' && currentRoomValue > 1) {
            currentRoomValue = currentRoomValue - 1

            if ((bookingResults['adults'] + bookingResults['children']) > (currentRoomValue * 4)) {
                var toDecreaseInAdultsAndChildrenDecrease = (bookingResults['adults'] + bookingResults['children']) - (currentRoomValue * 4)
                if (bookingResults['children'] >= toDecreaseInAdultsAndChildrenDecrease) {
                    bookingResults['children'] = bookingResults['children'] - toDecreaseInAdultsAndChildrenDecrease
                } else {
                    toDecreaseInAdultsAndChildrenDecrease = toDecreaseInAdultsAndChildrenDecrease - bookingResults['children']
                    bookingResults['children'] = 0
                    bookingResults['adults'] = bookingResults['adults'] - toDecreaseInAdultsAndChildrenDecrease
                }
            }
        }

        bookingResults['rooms'] = currentRoomValue
        this.setState({ bookingResults })
    }

    handleAdults = (control) => {
        const { bookingResults } = this.state
        var currentAdultValue = bookingResults['adults']

        if (control === 'increase' && currentAdultValue < 20) {
            if ((bookingResults['children'] + currentAdultValue) < 20) {
                currentAdultValue = currentAdultValue + 1
                if ((bookingResults['rooms'] * 4) < (bookingResults['children'] + currentAdultValue)) {
                    var toIncreaseInRooms = bookingResults['children'] + currentAdultValue - (bookingResults['rooms'] * 4)
                    if (toIncreaseInRooms <= 4) {
                        bookingResults['rooms'] = bookingResults['rooms'] + 1;
                    } else {
                        bookingResults['rooms'] = bookingResults['rooms'] + Math.ceil(toIncreaseInRooms / 4)
                    }
                }
            }
        }
        else if (control === 'decrease' && currentAdultValue > 1) {
            if (((bookingResults['children'] + currentAdultValue) <= (bookingResults['rooms'] * 4)) && ((currentAdultValue - 1) >= bookingResults['rooms'])) {
                currentAdultValue = currentAdultValue - 1
            } else if ((bookingResults['rooms'] === currentAdultValue) && (((bookingResults['rooms'] - 1) * 4) >= ((currentAdultValue - 1) + bookingResults['children']))) {
                currentAdultValue = currentAdultValue - 1
                bookingResults['rooms'] = bookingResults['rooms'] - 1
            }
        }

        bookingResults['adults'] = currentAdultValue
        this.setState({ bookingResults })
    }

    handleChildren = (control) => {
        const { bookingResults } = this.state
        var currentChildrenValue = bookingResults['children']

        if (control === 'increase' && currentChildrenValue < 15) {
            if ((bookingResults['adults'] + currentChildrenValue) < 20) {
                currentChildrenValue = currentChildrenValue + 1
                if ((bookingResults['rooms'] * 4) < (bookingResults['adults'] + currentChildrenValue)) {
                    var toIncreaseInRooms = bookingResults['adults'] + currentChildrenValue - (bookingResults['rooms'] * 4)
                    if (toIncreaseInRooms <= 4) {
                        bookingResults['rooms'] = bookingResults['rooms'] + 1;
                        if (bookingResults['rooms'] > bookingResults['adults']) {
                            bookingResults['adults'] = bookingResults['rooms']
                        }
                    } else {
                        bookingResults['rooms'] = bookingResults['rooms'] + Math.ceil(toIncreaseInRooms / 4)
                    }
                }
            }
        } else if (control === 'decrease' && currentChildrenValue > 0) {
            currentChildrenValue = currentChildrenValue - 1
        }

        bookingResults['children'] = currentChildrenValue
        this.setState({ bookingResults })
    }

    render() {
        const { bookingDetails } = this.props
        const { bookingResults } = this.state
        return (
            <div>
                {
                    bookingDetails.map((renderDetails, renderIndex) => {
                        return (
                            <div className={`individual-part ${renderIndex === 2 && 'no-border'}`} key={renderIndex}>
                                <div className="left-content">
                                    <div className="book-image">
                                        <img className="image-fit" src={renderDetails.categoryImage} alt="rooms" />
                                    </div>
                                    <div className="book-text">
                                        {renderDetails.category}
                                    </div>
                                </div>

                                <div className="right-content">
                                    <div className="book-image cursor-pointer" onClick={() => this.updateState(renderDetails.update_query, 'decrease')}>
                                        <img className="image-fit" src={Decrease} alt="decrease" />
                                    </div>
                                    <div className="count">{bookingResults[renderDetails.update_query]}</div>
                                    <div className="book-image cursor-pointer" onClick={() => this.updateState(renderDetails.update_query, 'increase')}>
                                        <img className="image-fit" src={Increase} alt="increase" />
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }

            </div>
        );
    }
}

export default BookingFunctionality