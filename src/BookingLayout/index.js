import React from 'react';
import ChoosePeople from '../assets/choose-people.svg';
import Adults from '../assets/adult.svg';
import Rooms from '../assets/room.svg';
import Children from '../assets/children.svg';
import BookingFunctionality from '../BookFunctionality';

require('./booking.scss')

class BookingLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bookingDetails: [
                { category: 'Rooms', categoryImage: Rooms, update_query: 'rooms' },
                { category: 'Adults', categoryImage: Adults, update_query: 'adults' },
                { category: 'Children', categoryImage: Children, update_query: 'children' }
            ]
        };
    }

    render() {
        const { bookingDetails } = this.state
        return (
            <div className="bookingComponent">
                <div className="bookingContainer">
                    <div className="head-content">
                        <div className="book-image">
                            <img className="image-fit" src={ChoosePeople} alt="choose-people" />
                        </div>
                        <div className="book-text">
                            Choose number of <strong>people</strong>
                        </div>
                    </div>

                    <div className="booking-feature">
                        <BookingFunctionality
                            bookingDetails={bookingDetails}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default BookingLayout