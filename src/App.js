import React from 'react';
import BookingLayout from './BookingLayout';
require('./App.scss')

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <div className="mainContainer">
        <BookingLayout />
      </div>
    );
  }
}

export default App