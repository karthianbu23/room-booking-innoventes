import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders room booking app', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Choose/i);
  expect(linkElement).toBeInTheDocument();
});
